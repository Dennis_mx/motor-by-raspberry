#include <linux/init.h>
#include <linux/module.h>
#include <linux/cdev.h>
#include <linux/fs.h>
#include <linux/device.h>
#include <linux/slab.h>
#include <asm/uaccess.h>
#include "dev_motor_head.h"
#include <linux/gpio.h> 

#include <linux/ktime.h>
#include <linux/kernel.h>
#include <linux/unistd.h>
#include <linux/delay.h>
#include <linux/kthread.h>

#define RESP_TIME       	50000   //(us)
#define PUL_MIN_US 			1
#define PUL_MAX_US 			1
#define IO_INIT 			0
#define MAX_MOTOR_NO 		3
#define MAX_CMD 			128

//指定的主设备号
#define MAJOR_NUM 			250 

// CMD "queue" node 
typedef struct {
	int					m_dir;
	unsigned long 		m_step;
	unsigned long 		m_ts;	
}cmd_node_t;

cmd_node_t  				cmd_queue[MAX_MOTOR_NO][MAX_CMD];
cmd_node_t *  				pCMDRead[MAX_MOTOR_NO];
cmd_node_t * 				pCMDWrite[MAX_MOTOR_NO];

int							motor_dir[MAX_MOTOR_NO];
uint32_t  					motor_step[MAX_MOTOR_NO];
uint32_t  					motor_ts[MAX_MOTOR_NO];
unsigned int				PUL_PIN[MAX_MOTOR_NO] = {26, 19, 13, 6, 5};
unsigned int				DIR_PIN[MAX_MOTOR_NO] = {21, 20, 16, 12, 25};
//unsigned int				ENA_PIN[MAX_MOTOR_NO] = {22, 27, 17, 4, 18};
static struct task_struct * control_kthred[MAX_MOTOR_NO]; 

struct gpio_chip * 			gpiochip;  

//自己的字符设备
struct mycdev 
{
    int 			len;
    unsigned char 	buffer[50];
    struct cdev 	cdev;
};
MODULE_LICENSE("GPL");

//设备号
static dev_t dev_num = {0};
//全局gcd 
struct mycdev * gcd;
//设备类
struct class * cls;

//获得用户传递的数据，根据它来决定注册的设备个数
static int ndevices = 1;
module_param(ndevices, int, 0644);
MODULE_PARM_DESC(ndevices, "The number of devices for register.\n");


uint16_t get_cmd_total(int nu)
{
	uint16_t len = 0;
    if (pCMDWrite[nu] < pCMDRead[nu])
    {
        len = (MAX_CMD - (pCMDRead[nu] - pCMDWrite[nu]));
    }
    else
    {
        len = (pCMDWrite[nu] - pCMDRead[nu]);
    }
	//printk(KERN_INFO "Get Motor_%d CMD Total: %ul\n", nu, len);
	return len;
}

int write_CMD_queue(int nu, cmd_node_t cmd)
{
	
	if (get_cmd_total(nu) == (MAX_CMD - 1))
    {
        //cmd_queue[nu] is full, no data can write in
        return -1;
    }
	pCMDWrite[nu]->m_dir = cmd.m_dir;
    pCMDWrite[nu]->m_step = cmd.m_step;
	pCMDWrite[nu]->m_ts = cmd.m_ts;
	printk(KERN_INFO "Write Motor_%d CMD %lu %lu; %lu\n", nu, pCMDWrite[nu]->m_dir, pCMDWrite[nu]->m_step, pCMDWrite[nu]->m_ts);
	
    if ((pCMDWrite[nu] - cmd_queue[nu]) == (MAX_CMD - 1))
    {
        pCMDWrite[nu] = cmd_queue[nu];
    }
    else
    {
        pCMDWrite[nu]++;
    }	
	return 0;
}
int read_CMD_queue(int nu, cmd_node_t * cmd)
{
	if (pCMDWrite[nu] == pCMDRead[nu])
    {
        //cmd_queue[nu] is empty, no data read
        return 0xEE;
    }
	cmd->m_dir = pCMDRead[nu]->m_dir;
    cmd->m_step = pCMDRead[nu]->m_step;
	cmd->m_ts	= pCMDRead[nu]->m_ts;
	printk(KERN_INFO "Read Motor_%d CMD :%lu %lu; %lu\n", nu, cmd->m_dir, cmd->m_step, cmd->m_ts);
	
    if ((pCMDRead[nu] - cmd_queue[nu]) == (MAX_CMD - 1))
    {
        pCMDRead[nu] = cmd_queue[nu];
    }
    else
    {
        pCMDRead[nu]++;
    }	
    return 0;
}

int clear_CMD_queue(int nu)
{
	pCMDRead[nu] 	= cmd_queue[nu];
	pCMDWrite[nu] 	= cmd_queue[nu];
	printk(KERN_INFO "Clear CMD Motor_NO:%d\n", nu);
return 0;	
}

//打开设备
static int dev_motor_open(struct inode *inode, struct file *file)
{
    struct mycdev *cd;
    
    printk(KERN_INFO "dev_motor_open success!\n");
    
    //用struct file的文件私有数据指针保存struct mycdev结构体指针
    cd = container_of(inode->i_cdev, struct mycdev, cdev);
    file->private_data = cd;
    
    return 0;
}

//读设备
static ssize_t dev_motor_read(struct file *file, char __user *ubuf, size_t size, loff_t *ppos)
{
    int n;
    int ret;
    char *kbuf;
    struct mycdev *mycd = file->private_data;
    
    printk(KERN_INFO "read *ppos : %lld\n", *ppos);

    if(*ppos == mycd->len)
        return 0;

    //请求大大小 > buffer剩余的字节数 :读取实际记得字节数
    if(size > mycd->len - *ppos)
        n = mycd->len - *ppos;
    else 
        n = size;
    
    printk(KERN_INFO "n = %d\n",n);
    //从上一次文件位置指针的位置开始读取数据
    kbuf = mycd->buffer + *ppos;

    //拷贝数据到用户空间
    ret = copy_to_user(ubuf, kbuf, n);
    if(ret != 0)
        return -EFAULT;
    
    //更新文件位置指针的值
    *ppos += n;
    
    printk(KERN_INFO "dev_motor_read success!\n");

    return n;
}


unsigned long str_to_uint(char s[])
{
	unsigned long ret = 0;
	char *p = s;

	while( *p != '\0' )
	{
		if((*p < '0') || (*p > '9'))
		{ p++; continue;}
		ret = ret * 10 + (unsigned int)(*p - '0');
		p++;
	}
	return ret;
}

static int motor_control(void * data)
{
	int motor_nu = *(int*)data;
	cmd_node_t new_cmd;
	
	printk(KERN_INFO "Motor_%d Contorl coming...\n", motor_nu);
	//init control GPIO
	
	 while(1)
     {
		//Set motor dir 
	/*	//kfifo_out(&fifo, buf, 1);		
		gpiochip->set(gpiochip, DIR_PIN[motor_nu], motor_dir[motor_nu]);
		usleep_rang(1, 2);
	*/	if(0 == get_cmd_total(motor_nu))
		{
			usleep_range(RESP_TIME-10, RESP_TIME);
		}
		else
		{
			read_CMD_queue(motor_nu, &new_cmd);
			motor_step[motor_nu] = new_cmd.m_step;
			motor_ts[motor_nu]	= new_cmd.m_ts;
			if(motor_dir[motor_nu] != new_cmd.m_dir)
			{
				gpiochip->set(gpiochip, DIR_PIN[motor_nu], new_cmd.m_dir);
				motor_dir[motor_nu] = new_cmd.m_dir;
				usleep_range(300, 400);
				printk(KERN_INFO "Motor_%d change DIR %lu\n", motor_nu, new_cmd.m_dir);
			}
			printk(KERN_INFO "Motor_%d Steps:%lu ts:%lu\n", motor_nu, motor_step[motor_nu], motor_ts[motor_nu]);			
		}
				
		while( motor_step[motor_nu] != 0 )
		{
			usleep_range(motor_ts[motor_nu]-1, motor_ts[motor_nu]);			
			gpiochip->set(gpiochip, PUL_PIN[motor_nu], 1);
			usleep_range(PUL_MIN_US, PUL_MAX_US);
			gpiochip->set(gpiochip, PUL_PIN[motor_nu], 0);
			motor_step[motor_nu] -= 1;						
		}
		motor_ts[motor_nu] = 0;
	} 
return 0;
}


void send_motor_cmd(char * kbuf)
{
	char * utoken;
	unsigned long motor_cmd[4] = {0,0,0,0};
	cmd_node_t cmd_tmp;

	utoken = strsep(&kbuf, ".");
	motor_cmd[0] = str_to_uint(utoken); 

	utoken = strsep(&kbuf, ".");
	motor_cmd[1] = str_to_uint(utoken); 

	utoken = strsep(&kbuf, ".");
	motor_cmd[2] = str_to_uint(utoken); 

	utoken = strsep(&kbuf, ".");
	motor_cmd[3] = str_to_uint(utoken); 
	
    printk(KERN_INFO "Send_CMD motor_%lu; speed:%lu; Steps:%lu\n", (motor_cmd[0] -1), motor_cmd[2], motor_cmd[3]);
	
	if (motor_cmd[0] > MAX_MOTOR_NO)
	{
		printk(KERN_INFO "Writed CMD Error! Motor No: %lu > [MAX_MOTOR_NO]\n", motor_cmd[0]);
	}
	else
	{
		if((motor_cmd[2] == 0) && (motor_cmd[3] == 0)) //This is STOP_CMD
		{
			clear_CMD_queue(motor_cmd[0] - 1);
			motor_step[motor_cmd[0] - 1] = 1;
			printk(KERN_INFO "STOP CMD motor_%lu;\n", (motor_cmd[0] - 1));
		}
		else
			{
				cmd_tmp.m_dir = motor_cmd[1];
				cmd_tmp.m_step = motor_cmd[2];
				cmd_tmp.m_ts = motor_cmd[3];
				write_CMD_queue(motor_cmd[0] - 1, cmd_tmp);
			}
	}		
}


//写设备
static ssize_t dev_motor_write(struct file *file, const char __user *ubuf, size_t size, loff_t *ppos)
{
    int n;
    int ret;
    char * kbuf;
	
    struct mycdev *mycd = file->private_data;	

    printk(KERN_INFO  "write *ppos : %lld\n", *ppos);
    
    //已经到达buffer尾部了
    if(*ppos == sizeof(mycd->buffer))
        return -1;

    //请求大大小 > buffer剩余的字节数(有多少空间就写多少数据)
    if(size > sizeof(mycd->buffer) - *ppos)
        n = sizeof(mycd->buffer) - *ppos;
    else 
        n = size;

    //从上一次文件位置指针的位置开始写入数据
    kbuf = mycd->buffer + *ppos;

    //拷贝数据到内核空间

    ret = copy_from_user(kbuf, ubuf, n);
    if(ret != 0)
        return -EFAULT;

    //更新文件位置指针的值
    *ppos += n;
    
    //更新dev_motor.len 
    mycd->len += n;
    printk(KERN_INFO "dev_motor_write buf:%s\n", kbuf);

	send_motor_cmd(kbuf);

    return n;
}

//linux 内核在2.6以后，已经废弃了ioctl函数指针结构，取而代之的是unlocked_ioctl
long dev_motor_unlocked_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{
    int ret = 0;
    
    switch(cmd)
    {
		case 0:   
			printk(KERN_INFO "CMD_0 coming!\n");
			break;
			
		case 1:  
			printk(KERN_INFO "CMD_1 down!\n");  
			break;

        default:
            return -EFAULT;
    }

    return ret;
}


//设备操作函数接口
static const struct file_operations motor_operations = {
    .owner 				= THIS_MODULE,
    .open 				= dev_motor_open,
    .read 				= dev_motor_read,
    .write 				= dev_motor_write,
    .unlocked_ioctl 	= dev_motor_unlocked_ioctl,
};

static int is_right_chip(struct gpio_chip *chip, void *data)  
{
    if (strcmp(data, chip->label) == 0)  
        return 1;  
    return 0;  
} 
 
void motor_init(void)
{
	int motor_nu;
	cmd_node_t cmd_tmp;
	
	cmd_tmp.m_dir = 1;
	cmd_tmp.m_step = 200;
	cmd_tmp.m_ts = 1000;	

	gpiochip = gpiochip_find("pinctrl-bcm2835", is_right_chip);  
	for (motor_nu = 0; motor_nu < MAX_MOTOR_NO; motor_nu++)
	{
		gpiochip->direction_output(gpiochip, PUL_PIN[motor_nu], 1);  
		gpiochip->direction_output(gpiochip, DIR_PIN[motor_nu], 1);  
	//	gpiochip->direction_output(gpiochip, ENA_PIN[motor_nu], 1);  
		gpiochip->set(gpiochip, PUL_PIN[motor_nu], IO_INIT);  
		gpiochip->set(gpiochip, DIR_PIN[motor_nu], IO_INIT);  
	//	gpiochip->set(gpiochip, ENA_PIN[motor_nu], IO_INIT);  
		
		pCMDRead[motor_nu] 	= cmd_queue[motor_nu];
		pCMDWrite[motor_nu] = cmd_queue[motor_nu];
		motor_dir[motor_nu] = 1;
		
		write_CMD_queue(motor_nu, cmd_tmp);
		
		do
		{			
			control_kthred[motor_nu]= kthread_run(motor_control, &motor_nu, "mythread%d", motor_nu);
			usleep_range(20, 200);			
		}while( IS_ERR(control_kthred[motor_nu]) );
		printk(KERN_INFO "Init create control_kthred %d ok~~\n", motor_nu);
	}	
}

void motor_remov(void)
{
	int motor_nu;
 
	for (motor_nu = 0; motor_nu < MAX_MOTOR_NO; motor_nu++)
	{		
		if (!IS_ERR(control_kthred[motor_nu]))
		{  
			int ret = kthread_stop(control_kthred[motor_nu]);
			printk(KERN_INFO "thread function has run %ds\n", ret);  
		}
		
		gpio_free(PUL_PIN[motor_nu]);
		gpio_free(DIR_PIN[motor_nu]);
	//	gpio_free(ENA_PIN[motor_nu]);		

		printk(KERN_INFO "Motor_%d remove ok!\n", motor_nu);
	}	
}


//模块入口
int __init dev_motor_init(void)
{
    int i = 0;
    int n = 0;
    int ret;
    struct device *device;
    
    gcd = kzalloc(ndevices * sizeof(struct mycdev), GFP_KERNEL);
    if(!gcd){
        return -ENOMEM;
    }

    //设备号 : 主设备号(12bit) | 次设备号(20bit)
    dev_num = MKDEV(MAJOR_NUM, 0);

    //静态注册设备号
    ret = register_chrdev_region(dev_num, ndevices, "dev_motor");
    if(ret < 0){

        //静态注册失败，进行动态注册设备号
        ret = alloc_chrdev_region(&dev_num, 0, ndevices, "dev_motor");
        if(ret < 0){
            printk(KERN_INFO "Fail to register_chrdev_region\n");
            goto err_register_chrdev_region;
        }
    }
    
    //创建设备类
    cls = class_create(THIS_MODULE, "dev_motor");
    if(IS_ERR(cls)){
        ret = PTR_ERR(cls);
        goto err_class_create;
    }
    
    printk(KERN_INFO "ndevices : %d\n",ndevices);
    
    for(n = 0; n < ndevices; n++)
    {
        //初始化字符设备
        cdev_init(&gcd[n].cdev, &motor_operations);

        //添加设备到操作系统
        ret = cdev_add(&gcd[n].cdev, dev_num + n, 1);
        if (ret < 0)
        {
            goto err_cdev_add;
        }
        //导出设备信息到用户空间(/sys/class/类名/设备名)
        device = device_create(cls, NULL, dev_num + n, NULL, "dev_motor%d", n);
        if(IS_ERR(device))
		{
            ret = PTR_ERR(device);
            printk(KERN_INFO "Fail to device_create\n");
            goto err_device_create;    
        }
    }
	motor_init();
    printk(KERN_INFO "Register dev_fito to system,ok!\n");
    
    return 0;

err_device_create:
    //将已经导出的设备信息除去
    for(i = 0; i < n; i++)
    {
        device_destroy(cls, dev_num + i);    
    }

err_cdev_add:
    //将已经添加的全部除去
    for(i = 0;i < n;i ++)
    {
        cdev_del(&gcd[i].cdev);
    }

err_class_create:
    unregister_chrdev_region(dev_num, ndevices);

err_register_chrdev_region:

    return ret;
}

void __exit dev_motor_exit(void)
{
    int i;
	
	motor_remov();

    //删除sysfs文件系统中的设备
    for(i = 0; i < ndevices;  i++)
    {
        device_destroy(cls, dev_num + i);    
    }

    //删除系统中的设备类
    class_destroy(cls);
 
    //从系统中删除添加的字符设备
    for(i = 0; i < ndevices; i++)
    {
        cdev_del(&gcd[i].cdev);
    }
    
    //释放申请的设备号
    unregister_chrdev_region(dev_num, ndevices);
	printk(KERN_INFO "Dev_motor exit ok!\n");

    return;
}


module_init(dev_motor_init);
module_exit(dev_motor_exit);
