
MOD=dev_motor

obj-m += $(MOD).o
KDIR := /home/dev/pi3Linux/linux   #内核源码的路径
PWD = $(shell pwd)

all:
	make -C $(KDIR) M=$(PWD) modules ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf-

clean:
	rm -rf $(MOD).mod.c $(MOD).mod.o $(MOD).o 
